# MTMW12_assig3

Assignment 3 for Numerical Modelling

For part 1)
* Run geoParamters.py, differentiate.py and geostrophicWind.py

* Change paramters inside the geoParameters.py code

* Change N inside the geostrophicWind.py code. This code includes two functions  
for a 2nd and 4th order accuracy numerical differentiation. N should be 
changed inside each function.

* The first 2 graphs produced by geostrophicWind.py will be the 2nd order, and
the last 2 will be 4th order

*  To produce the log(error) against log(dy) plot, run dyerror.py

*  The number of terms looked at is terms*10. The parameter terms should be 
entered in the errorplot() function.

For part 2)
* Run moreterms_differentiate.py

* Run geostrophicWind.py

* to compare the 2 different accuracies, use combinegraphs.py, which performs the
 code inside the geostrophicWind() function in geostrophicWind.py for each
accuracy and plots the solutions and errors for each on the same graphs