# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:14:54 2019

@author: af802403
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from moreterms_differentiate import *

def geostrophicWind():
    "Sets the N value and therefore the dy value."
    "Calls other functions and parameter values in order to plot the"
    "analytical values and 2nd order accuracy numerical approximation values"
    "of wind at a chosen pressure and distance range"
    #input parameters describing problem
    import geoParameters as gp
    
    #resolution
    N=10
    dy = (gp.ymax - gp.ymin)/N   #length of the spacing
    
    y = np.linspace(gp.ymin, gp.ymax, N+1)   #the spatial dimension

    #the geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    #the pressure at the y points
    p = gp.pressure(y)
    
    #The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p,dy)
    u_2point = gp.geoWind(dpdy)
    
    #the difference between the wind values using two point differences and
    #the exact values
    error = np.zeros_like(u_2point)
    for j in range(0,len(u_2point)):
        error[j] = abs( u_2point[j] - uExact[j] )
    
    font={'size':14}
    plt.rc('font', **font)
    
    #graph to compare numerical and analytic solutions
    plt.figure(1)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Two point differences',\
             ms=12, markeredgewidth=1.5,markerfacecolor='none')
    plt.errorbar(y/1000, u_2point, yerr=error, linestyle=None, ecolor='red')
    plt.legend(loc='best')
    #plt.xlim(-1000,6000)
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindCentN50.pdf')
    plt.show()
    
    #graph plotting error for each y value
    plt.figure(2)
    plt.plot(y/1000, error, 'x')
    plt.ylim(0,max(error)*1.1)
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('plots/geoWindErrorCentN50.pdf')
    plt.show()


def fourthgeostrophicWind():
    "Sets the N value and therefore the dy value."
    "Calls other functions and parameter values in order to plot the"
    "analytical values and 4th order accuracy numerical approximation values"
    "of wind at a chosen pressure and distance range"
    #input parameters describing problem
    import geoParameters as gp
    
    N=10    #resolution
    dy = (gp.ymax - gp.ymin)/N   #length of the spacing
    
    y = np.linspace(gp.ymin, gp.ymax, N+1)   #the spatial dimension
    #the geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    #the pressure at the y points
    p = gp.pressure(y)
    
    #The pressure gradient and wind using two point differences
    dpdy = moreterms_2point(p,dy)
    u_2point = gp.geoWind(dpdy)
    
    #the difference between the wind values using two point differences and
    #the exact values
    error = np.zeros_like(u_2point)
    for j in range(0,len(u_2point)):
        error[j] = abs( u_2point[j] - uExact[j] )
    
    font={'size':14}
    plt.rc('font', **font)
    
    #graph to compare numerical and analytic solutions
    plt.figure(1)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k--', label='Two point differences',\
             ms=12, markeredgewidth=1.5,markerfacecolor='none')
    plt.errorbar(y/1000, u_2point, yerr=error, linestyle=None, ecolor='red')
    plt.legend(loc='lower left')
    #plt.xlim(-1000,6000)
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('plots/4thgeoWindCentN10.pdf')
    plt.show()
    
    #graph plotting error for each y value
    plt.figure(2)
    plt.plot(y/1000, error, 'x')
    plt.ylim(0,max(error)*1.1)
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('plots/4thgeoWindErrorCentN10ignoreoutliers.pdf')
    plt.show()

if __name__=="__main__":
    geostrophicWind()
    fourthgeostrophicWind()