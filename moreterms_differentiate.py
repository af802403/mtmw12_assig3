# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 15:42:04 2019

@author: af802403
"""

import numpy as np

#Funtion for calculating gradients

def moreterms_2point(f,dx):
    "The gradient of array f assuming points are a distance dx apart"
    "using 2-point differences to 4th order accuracy"
    import geoParameters as gp
    #initialise the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points
    dfdx[1] = (4*f[2] - 3*f[1] - f[3])/(2*dx)
    dfdx[0] = (4*f[1] - 3*f[0] - f[2])/(2*dx)
    
    dfdx[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(2*dx)
    dfdx[-2] = (3*f[-2] - 4*f[-3] + f[-4])/(2*dx)
    
    #centre differences for the mid-points
    for i in range(2,len(f)-2):
        dfdx[i] = (f[i-2] - f[i+2] + 8*f[i+1] - 8*f[i-1])/(12*dx)
    return dfdx