# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:11:33 2019

@author: af802403
"""

import numpy as np

#Funtion for calculating gradients

def gradient_2point(f,dx):
    "The gradient of array f assuming points are a distance dx apart"
    "using 2-point differences to second order accuracy"
    import geoParameters as gp
    #initialise the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    
    #centre differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    return dfdx
